# Vanilla JavaScript - Multiple Use Filters

Content filtering for multiple use on a single page. By default, the filters utilize a masonry style layout.

You will find, underneath the public folder, examples and the distribution code to use. If you'd like to look into the code as it was created, feel free to check out the src folder.

The intended use for this script is on the JcInk forums for RPGs or similar RPG sites. Please maintain credit. I'd also love for you to share with me anything and everything that you create! I enjoy seeing everyone's work. :) If you have any questions, feel free to contact me at jessica@jessicastamos.com.